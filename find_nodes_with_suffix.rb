require "json"

class Tree
  attr_reader :root

  def initialize(tree_data)
    @root = TreeNode.new(tree_data)
  end

  def find_nodes_with_suffix(suffix)
    @root.find_nodes_with_suffix(suffix)
  end

  def find_path_to_nodes_with_suffix(suffix)
    @root.find_path_to_nodes_with_suffix(suffix)
  end
end

class TreeNode
  attr_reader :name
  attr_reader :children

  def initialize(node_data)
    @name = node_data["text"]
    @children = []
    nodes = node_data["children"]
    nodes.each { |node| @children << TreeNode.new(node) }
  end

  def find_nodes_with_suffix(suffix)
    result = []
    result << self if @name.end_with?(suffix)
    result += @children.flat_map { |child| child.find_nodes_with_suffix(suffix) }
  end

  def find_path_to_nodes_with_suffix(suffix, path = [])
    result = []
    path << @name
    result << path.dup if @name.end_with?(suffix)
    result += @children.flat_map { |child| child.find_path_to_nodes_with_suffix(suffix, path.dup) }
  end
end

json = File.read("tree.json")
tree_data = JSON.parse(json)
tree = Tree.new(tree_data)

puts tree.find_nodes_with_suffix("PV").map { |node| node.name }

pp tree.find_path_to_nodes_with_suffix("PV")