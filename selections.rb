require "json"

CONDITION = {
  :variable => "FC304.PV",
  :min => 76.75,
  :max => 76.80
}

def get_selection_timestamp(dataset)
  variable_index = dataset["variables"].index(CONDITION[:variable])
  dataset["data"].filter_map { |data_row| data_row[0] if data_row[variable_index].between?(CONDITION[:min], CONDITION[:max]) }
end

def get_data_for_selection(dataset, timestamps)
  timestamp_hashes = timestamps.to_set
  dataset["data"].filter { |data_row| timestamp_hashes === data_row[0] }
end

json = File.read("dataset.json")
dataset = JSON.parse(json)

puts timestamps = get_selection_timestamp(dataset)

pp get_data_for_selection(dataset, timestamps)